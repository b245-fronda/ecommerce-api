const Product = require("../Models/productsSchema.js");
const Order = require("../Models/ordersSchema.js");
const Cart = require("../Models/cartSchema.js");
const auth = require("../auth.js");

// Create product (Admin only)
module.exports.addProduct = (request, response) => {
	let input = request.body;
	const userData = auth.decode(request.headers.authorization);
	if(userData.isAdmin === false){
		return response.send("Access denied. Not an admin.")
	}
	else{
		Product.findOne({name: input.name})
		.then(result => {
			if(result !== null){
				return response.send("Product already exists!")
			}
			else{
				let newProduct = new Product({
					name: input.name,
					description: input.description,
					price: input.price
				})

				newProduct.save()
				.then(save => response.send("Product added!"))
				.catch(error => response.send(error))
			}
		})
		.catch(error => response.send(error))
	}
};

// Retrieve all active products
module.exports.allActiveProducts = (request, response) => {
	Product.find({isActive: true})
	.then(result => response.send(result))
	.catch(error => response.send(error))
};

// Retrieve single product
module.exports.productDetails = (request, response) => {
	const productId = request.params.productId;
	Product.findById(productId)
	.then(result => response.send(result))
	.catch(error => response.send(error))
};

// Update product information (Admin only)
module.exports.updateProduct = (request, response) => {
	let input = request.body;
	const productId = request.params.productId;
	const userData = auth.decode(request.headers.authorization);
	if(userData.isAdmin === false){
		return response.send("Access denied. Not an admin.")
	}
	else{
		Product.findById(productId)
		.then(result => {
			if(result === null){
				return response.send("Invalid productId.")
			}
			else{
				let updatedProduct = {
					name: input.name,
					description: input.description,
					price: input.price
				}
				Product.findByIdAndUpdate(productId, updatedProduct, {new: true})
				.then(result => response.send(result))
				.catch(error => response.send(error))
			}
		})
		.catch(error => response.send(error))
	}
};

// Archive product (Admin only)
module.exports.archiveProduct = (request, response) => {
	let input = request.body;
	const productId = request.params.productId;
	const userData = auth.decode(request.headers.authorization);
	if(userData.isAdmin === false){
		return response.send("Acess denied. Not an admin.")
	}
	else{
		Product.findById(productId)
		.then(result => {
			if(productId === null){
				return response.send("Invalid productId.")
			}
			else{
				let archivedProduct = {
					isActive: input.isActive
				}
				Product.findByIdAndUpdate(productId, archivedProduct, {new: true})
				.then(result => response.send(result))
				.catch(error => response.send(error))
			}
		})
		.catch(error => response.send(error))
	}
};

// Non-admin user checkout (Create order)
module.exports.createOrder = (request, response) => {
	let input = request.body;
	const productId = request.params.productId;
	const userData = auth.decode(request.headers.authorization);
	if(userData.isAdmin === true){
		return response.send("User is an admin. Cannot create order.")
	}
	else{
		Product.findById(productId)
		.then(product => {
			let newOrder = new Order({
				userId: userData._id,
				totalAmount: input.quantity * product.price,
				products: {
					productId: productId,
					quantity: input.quantity
				}
			})
			newOrder.save()
			.then(order => response.send("Order created!"))
			.catch(error => response.send(error))
		})
		.catch(error => response.send(error))
	}
};

// Retrieve all orders (Admin only)
module.exports.allOrders = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	if(userData.isAdmin === false){
		return response.send("Access denied. Not an admin")
	}
	else{
		Order.find({})
		.then(result => response.send(result))
		.catch(error => response.send(error))
	}
};

// Add to cart
module.exports.addToCart = (request, response) => {
	let input = request.body;
	const userData = auth.decode(request.headers.authorization);
	// check existing cart for the user
	Cart.findOne({userId: userData._id})
	.then(cart => {
		// create new cart
		if(cart === null){
			Product.findById(input._id)
			.then(product => {
				if(product === null){
					return response.send("Product not found.")
				}
				else{
					let newCart = new Cart({
						userId: userData._id,
						products: [{
							productId: input._id,
							name: product.name,
							quantity: input.quantity,
							price: product.price
						}],
						totalAmount: input.quantity * product.price
					})
					newCart.save()
					.then(save => response.send("Added to cart!"))
					.catch(error => response.send(error))
				}
			})
			.catch(error => response.send(error))
		}
		// add to cart (push)
		else{
			Product.findById(input._id)
			.then(product => {
				if(product === null){
					return response.send("Product not found.")
				}
				else{
					cart.products.push({
						productId: input._id,
						name: product.name,
						quantity: input.quantity,
						price: product.price
					})
					let totalAmount = 0;
					for (let i = 0; i < cart.products.length; i++) {
					        totalAmount += cart.products[i].price * cart.products[i].quantity;
					    }
					cart.totalAmount = totalAmount;
					cart.save()
					.then(save => response.send("Added to cart!"))
					.catch(error => response.send(error))
				}
			})
			.catch(error => response.send(error))
		}
	})
	.catch(error => response.send(error))
};