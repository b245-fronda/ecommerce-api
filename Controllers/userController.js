const mongoose = require("mongoose");
const bcrypt = require("bcrypt");

const User = require("../Models/usersSchema.js");
const Order = require("../Models/ordersSchema.js");
const auth = require("../auth.js");

// User registration
module.exports.userRegistration = (request, response) => {
	let input = request.body;
	User.findOne({email: input.email})
	.then(result => {
		if(result !== null){
			return response.send("The email is already taken!")
		}
		else{
			let newUser = new User({
				firstName: input.firstName,
				lastName: input.lastName,
				email: input.email,
				password: bcrypt.hashSync(input.password, 10)
			})

			newUser.save()
			.then(save => response.send("You are now registered!"))
			.catch(error => response.send(error))
		}
	})
	.catch(error => response.send(error))
};

// User authentication
module.exports.userAuthentication = (request, response) => {
	let input = request.body;
	User.findOne({email: input.email})
	.then(result => {
		if(result === null){
			return response.send("Email is not registered!")
		}
		else{
			const isPasswordCorrect = bcrypt.compareSync(input.password, result.password)
			if(isPasswordCorrect){
				return response.send({auth: auth.createAccessToken(result)});
			}
			else{
				return response.send("Password is incorrect!")
			}
		}
	})
	.catch(error => response.send(error));
};

// User details
module.exports.userDetails = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	User.findById(userData._id)
	.then(result => {
		result.password = "hidden";
		return response.send(result);
	})
};

// Set user as admin (Admin only)
module.exports.setAsAdmin = (request, response) => {
	const input = request.body;
	const userId = request.params.userId;
	const userData = auth.decode(request.headers.authorization);
	if(userData.isAdmin === false){
		return response.send("Acess denied. Not an admin.")
	}
	else{
		User.findById(userId)
		.then(result => {
			if(result === null){
				return response.send("User Id not found.")
			}
			else if(result.isAdmin === true){
				return response.send("User is already an admin.")
			}
			else{
				let newAdmin = {
					isAdmin: input.isAdmin
				}
				User.findByIdAndUpdate(userId, newAdmin, {new: true})
				.then(result => response.send("User is now an admin."))
				.catch(error => response.send(error))
			}
		})
		.catch(error => response.send(error))
	}
};

// Retrieve authenticated user's orders
module.exports.userOrders = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	const userId = request.params.userId;
	Order.find({userId: userId})
	.then(result => {
		if(userId === userData._id){
			if(result === null){
				return response.send("You have no orders yet.")
			}
			else{
				return response.send(result)
			}
		}
		else if(userData.isAdmin === true){
			if(result === null){
				return response.send("User has no orders yet.")
			}
			else{
				return response.send(result)
			}
		}
		else{
			return response.send("Acess denied")
		}
	})
	.catch(error => response.send(error))
};