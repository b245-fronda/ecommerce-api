const mongoose = require("mongoose");

const cartSchema = new mongoose.Schema({
	userId: {
		type: String,
		required: [true, "User Id is required"]
	},
	products: [
		{
			productId: {
				type: String,
				required: [true, "Product Id is required"]
			},
			name: {
				type: String,
				required: [true, "Product name is required"]
			},
			quantity: {
				type: Number,
				required: [true, "Product quantity is required"]
			},
			price: {
				type: Number,
				required: [true, "Price is required"]
			}
		}
	],
	totalAmount: {
		type: Number,
		required: [true, "Total amount is required"]
	}
});

module.exports = mongoose.model("Cart", cartSchema);