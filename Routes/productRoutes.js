const express = require("express");
const router = express.Router();
const productController = require("../Controllers/productController.js");
const auth = require("../auth.js");

// Routes without params
// Create product (Admin only)
router.post("/addProduct", auth.verify, productController.addProduct);
// Retrieve all active products
router.get("/allActiveProducts", productController.allActiveProducts);
// Retrieve all orders (Admin only)
router.get("/allOrders", auth.verify, productController.allOrders);
// Add to cart
router.post("/addToCart", auth.verify, productController.addToCart);

// Routes with params
// Retrieve single product
router.get("/:productId", productController.productDetails);
// Update product information (Admin only)
router.put("/update/:productId", auth.verify, productController.updateProduct);
// Archive product (Admin only)
router.put("/archive/:productId", auth.verify, productController.archiveProduct);
// Non-admin user checkout (Create order)
router.post("/order/:productId", auth.verify, productController.createOrder);

module.exports = router;