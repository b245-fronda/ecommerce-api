const express = require("express");
const router = express.Router();
const auth = require("../auth.js");

const userController = require("../Controllers/userController.js");

// Routes without params
// User registration
router.post("/register", userController.userRegistration);
// User authentication
router.post("/login", userController.userAuthentication);
// Retrieve user details
router.get("/details", auth.verify, userController.userDetails);

// Routes with params
// Set user as admin (Admin only)
router.put("/setAsAdmin/:userId", auth.verify, userController.setAsAdmin);
// Retrieve authenticated user's orders
router.get("/orders/:userId", auth.verify, userController.userOrders);

module.exports = router;