const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const port = 4000;
const app = express();

const userRoutes = require("./Routes/userRoutes.js");
const productRoutes = require("./Routes/productRoutes.js");

mongoose.set("strictQuery", true);
mongoose.connect("mongodb+srv://admin:admin@batch245-fronda.yatagmx.mongodb.net/Ecommerce_API?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;

db.on("error", console.error.bind(console, "Connection Error!"));

db.once("open", () => console.log("We are connected to the cloud!"));

app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());

app.use("/user", userRoutes);
app.use("/product", productRoutes);

app.listen(port, () => console.log(`Server is running at port ${port}`));